class RecipeFood(_name: String){
    val name = _name                                //properti nama resep yang akan dibuat
    var daftarBahanResep = listOf<String>()         //List Bahan dari resep yang dibuat
    var teknik: String? = null                      //digunakan untuk menentukan cara memasak
    var bahanUtama: String? = null                  //Menentukan Bahan Utama Resep yang dibuat

    //Class yang berisikan daftar bahan umum secara default
    class bahanUmum{
        val daftarBahan = listOf("Kentang", "Daging Ayam", "Daging Sapi", "Bawang Merah", "Daging Ikan", "Nasi")
    }
    //class yang digunakan untuk menentukan teknik memasak yang digunakan dari properti teknik
    inner class caraMemasak(){
        var teknikMemasak = listOf("Goreng","Panggang", "Bakar", "Rebus")
        var _teknik = teknik                                                //var teknik dapat diakses dari inner class
            get() = field
            set(value) {
                field = when(value){
                    "1" -> teknikMemasak[value.toInt()-1]
                    "2" -> teknikMemasak[value.toInt()-1]
                    "3" -> teknikMemasak[value.toInt()-1]
                    "4" -> teknikMemasak[value.toInt()-1]
                    else -> "Tdak memakai Teknik apapun"
                }
            }
    }
}