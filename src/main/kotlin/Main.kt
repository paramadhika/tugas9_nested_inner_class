fun main(args: Array<String>) {
    println("*======[ Make Your Own Recipe ]======*")
    print("\nMasukkan Nama Resep Makanan : ")
    val namaResep: String = readLine().toString()
    val resep1 = RecipeFood(namaResep)                                      //Membuat Objek resep1
    val bahanBahan = resep1.daftarBahanResep.toMutableList()                //membuat objek daftar bahan resep yang muteable
    println("\nPilih Bahan-Bahan yang diperlukan dari daftar berikut : ")
    val listBahan = RecipeFood.bahanUmum()                                  //membuat objek dari class bahan umum
    for(i in 0..listBahan.daftarBahan.size-1){                              //menampilkan daftar default dari list daftar bahan class bahan umum
        println("${i + 1}. ${listBahan.daftarBahan[i]}")
    }
    //Perulangan yang digunakan untuk memasukkan bahan-bahan yang diperlukan untuk membuat resep1
    loop@do{
        print("Pilih nomer diatas atau Tambahkan Bahan Anda Sendiri >>>>> ")
        val tambahBahan = readLine()!!
        val pilihan = tambahBahan.toIntOrNull()
        if(pilihan == null) {                                                                           //cek variabel pilihan berupa null ataupun integer
            do {                                                                                        //Jika berupa null maka inputan user pada var tambahBahan list bahan
                print("Yakin Menambahkan Bahan $tambahBahan ke Daftar Resep? \n1.ya\t2.tidak >>> ")     // akan ditambah kedalam apabila user memilih "ya" pada pilihan selanjutnya
                val pilih = readLine()
                if (pilih == "1" || pilih == "ya") {
                    bahanBahan.add(tambahBahan)
                    break
                }
                else if (pilih == "2" || pilih == "no") break
                else "Pilihan Tidak Dikenali"
            } while (true)                                      //<<<<< Jika "tidak", maka keluar dari perulangan ini <<<<<
            resep1.daftarBahanResep = bahanBahan                //Menambahkan daftar list bahanBahan ke objek resep1 di properti daftarBahanResep
        }
        //jika inputan user berupa integer maka tambahkan bahan yang terdapat pada list bahanUmum indeks [pilihan] ke list bahanBahan
        else if((pilihan in pilihan..listBahan.daftarBahan.size) && (pilihan-1 >= 0)){
            bahanBahan.add(listBahan.daftarBahan[pilihan-1])
        }
        else print("Tidak ada bahan yang anda pilih!!!\n")
        ulang@ do {                                                                     //perulangan untuk menentukan letak perulangan
            println("Masih Ingin Menambahkan Bahan?\n 1.ya\t2.tidak >>>  ")
            val ulangTambah = readLine()!!
            if (ulangTambah == "1" || ulangTambah.lowercase() == "ya") continue@loop    //jika "ya" maka ulang perulangan dari label @loop
            else if (ulangTambah == "2" || ulangTambah == "no") break@loop              //jika "tidak" maka stop perulangan dengan label @loop
            else {
                print("Pilihan tidak dikenali!!! ")                                     //jika selain nilai yang diminta, maka ulang perulangan
                continue@ulang                                                          //pada label @ulang
            }
        }while(true)
    }while(true)

    resep1.daftarBahanResep = bahanBahan

    //Proses untuk memilih bahan utama
    println("\nPilih Bahan Utama Anda : ")
    for(i in 0..bahanBahan.size-1){             //mencetak bahan-bahan yang telah dimasukkan sebelumnya
        print("${i + 1}. ${bahanBahan[i]} ")
    }
    println("Ketikkan Bahan Utama >>>> ")       //menginputkan bahan utama
    var bahanUtama = readLine()!!.toString()
    while(isBahanUtama(bahanUtama, bahanBahan) == false){       //pemanggilan functio isBahanUtama() untuk mengetahui apakah variabel inputan user
        print("Ketikkan Kembali Bahan Utama Anda!!!\n>>>>")     //berada dalam list bahanBahan
        bahanUtama = readLine()!!.toString()                    //perulangan diulang selama inputan user tidak terdapat pada list bahanBahan
    }
    resep1.bahanUtama = bahanUtama                              //Mengupdate bahan utama dari resep1

    println("\nPilih Teknik Memasak Untuk Mengolah $bahanUtama Anda : ")    //Proses untuk menetukan cara mengolah bahan utama
    var caraMasak = resep1.caraMemasak()                                    //membuat objek cara masak
    for(i in 0..caraMasak.teknikMemasak.size-1){                            //perulangan untuk mencetak daftar teknikMemasak pada objek caraMasak
        print("${i + 1}. ${caraMasak.teknikMemasak[i]} ")
    }
    do {
        print(">>> ")
        resep1.teknik = readLine()!!                                        //Menginputkan nomer teknik masak yang digunakan sesuai list yang
        caraMasak._teknik = resep1.teknik                                   //dicetak sebelumnya
        if(resep1.teknik!!.toInt() in 0 .. caraMasak.teknikMemasak.size-1) break //jika inputan sudah berada pada jangkauan 0 sampai 4 maka hentikan perulangan
        else continue                                                       //jika tidak ulangi perulangan
    }while(true)


    //Proses untuk mencetak objek resep1 beserta dengan properti-propertinya yang telah diinputkan
    println("\n\n============[ Resep Spesial ${resep1.name} ]============")
    print("Bahan-Bahan yang digunakan  : ")
    resep1.daftarBahanResep.forEach{print(" $it ")}
    println("\nBahan Utama yang dipillih   : ${resep1.bahanUtama}")
    println("Teknik yang dipilih untuk mengolah bahan utama  : ${caraMasak._teknik}")
}

fun isBahanUtama(utama: String, list: List<String>): Boolean{       //function yang digunakan untuk mengecek apakah bahan inputan user
    return list.contains(utama)                                     //terdapat pada list, jika "ya" return true, jika sebaliknya return false
}


